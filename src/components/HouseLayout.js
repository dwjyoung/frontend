import React, { useEffect, useState } from "react";
import ReactDom from "react-dom";
import houselayout from "../assets/house-layout.jpeg";
import ControlPage from "../pages/ControlPage";

function Dot(props) {
  var circleStyle = {
    padding: 3,
    margin: 20,
    display: "inline-block",
    backgroundColor: "#297373",
    borderRadius: "50%",
    width: 4,
    height: 4,
  };

  return (
    <div style={circleStyle}>
      <circle cx={props.x} cy={props.y} />
    </div>
  );
}

function HouseLayout() {
  let [{ x, y }, setXandY] = useState([]);

  let circleArray = [];

  const _onMouseClick = (e) => {
    setXandY({ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY });
    circleArray.push(<Dot cx={x} cy={y}></Dot>);
    console.log(circleArray)
  };

  return (
    <div>
      <div>
        <div className="item3">
          <div className="grid-container-house-layout dark">
          <div className="item3">
              <img
                width="1248"
                height="702"
                alt="house-Layout"
                src={houselayout}
                onClick={_onMouseClick.bind(this)}
              ></img>
              </div>
          </div>
        </div>
      </div>
      <h1>
        {x} {y}
      </h1>
    </div>
  );
}

export default HouseLayout;
