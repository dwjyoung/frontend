import Chart from "react-apexcharts";
import React, { Component } from "react";

export default class PowerChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          id: "apexchart-example",
        },
        xaxis: {
          categories: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ],
        },
      },
      series: [
        {
          name: "series-1",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        },
      ],
      sensors: [],
    };
    this.simulate = this.simulate.bind(this);
    this.getSensors = this.getSensors.bind(this);
  }
  componentDidMount() {
    this.getSensors();
  }
  simulate() {
    var totalPower = 0;
    var powerLightBulb = 31500;
    var powerHVAC = 525000;

    totalPower = powerLightBulb + powerHVAC;
    this.setState({
      series: [
        {
          name: "series-1",
          data: [0, 0, 0, totalPower / 1000, 0, 0, 0, 0, 0, 0, 0, 0],
        },
      ],
    });
  }

  getSensors = async () => {
    let response = await fetch("/api/microwave");
    let data = await response.json();
    let cost = data
    this.setState({ sensors: data });
    console.log(data);
  };

  render() {
    return (
      <div>
        <h1 className="smart-home-list-item"> Power Consumption </h1>
        <Chart
          options={this.state.options}
          series={this.state.series}
          type="bar"
          width={500}
          height={320}
          align="center"
        />
        <button onClick={this.simulate}>{<h2>Simulate</h2>}</button>
      </div>
    );
  }
}
