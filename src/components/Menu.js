import React from "react";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import HomePage from "../pages/HomePage";
import InfoPage from "../pages/InfoPage";
import ControlPage from "../pages/ControlPage";

function Menu() {
  return (
    <div className="item2">
      <div className="smart-home">
        <div className="smart-home-header">
          <h2 className="smart-home-title ">&#9782; Menu</h2>
        </div>
      </div>
      
        <div className="smart-home-list-item ">
          <Link to="/">
            <h3 className="">Home Page</h3>
          </Link>
          
        </div>
        <div className="smart-home-list-item">
          <Link to="/info">
            <h3>Info Page</h3>
          </Link>
          
        </div>
        <div className="smart-home-list-item">
          <Link to="/control">
            <h3>Control Page</h3>
          </Link>
        </div>
    </div>
  );
}

export default Menu;
