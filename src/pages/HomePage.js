import React from "react";
import HouseLayout from "../components/HouseLayout";
import Menu from "../components/Menu";
import Header from "../components/Header";
import ChangeTemp from "../components/ChangeTemp"

function HomePage() {
  // let [ACUnits, setACUnits] = useState([]);

  // useEffect(() => {
  //   getACUnits();
  // }, []);

  // let getACUnits = async () => {
  //   let response = await fetch("/api/ac-units");
  //   let data = await response.json();
  //   console.log("DATA:", data);
  //   setACUnits(data);
  // };

  return (
    <>
      
      <div className="grid-container dark">
        <Header />
        <Menu />
        <HouseLayout />
        <ChangeTemp />
      </div>
    </>
  );
}

export default HomePage;
