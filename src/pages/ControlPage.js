import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import Menu from "../components/Menu";
import ToggleSwitch from "../components/ToggleSwitch";

const ControlPage = ({ match, history }) => {
  let lightId = match.params.id;
  let [lights, setLights] = useState([]);
  let [light, setLight] = useState(null);

  useEffect(() => {
    getLights();
  }, []);

  let getLights = async () => {
    let response = await fetch("/api/light-bulb");
    let data = await response.json();
    setLights(data);
  };

  let getLight = async () => {
    if (lightId === "new") return;

    let response = await fetch(`/api/light-bulb/${lightId}/`);
    let data = await response.json();
    setLight(data);
  };

  let updateSensor = async () => {
    fetch(`/api/light-bulb/${lightId}/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(light.open_close),
    });
  };

  let handleSubmit = () => {
    updateSensor();
    console.log("you clicked me");
  };

  return (
    <>
      <div className="grid-container dark">
        <Header />
        <Menu />
        <div className="item3">
          <div className="grid-container dark">
            {lights.map((light, index) => (
              <ToggleSwitch
                onClick={handleSubmit}
                onchange={(e) => {
                  setLight({ ...light, body: e.target.value });
                }}
                key={index}
                light={light}
              />
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default ControlPage;
