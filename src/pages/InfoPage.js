import React from "react";
import Header from "../components/Header";
import Menu from "../components/Menu";
import PowerChart from "../components/PowerChart";
import WaterChart from "../components/WaterChart";
import CostChart from "../components/CostChart";

const InfoPage = (props) => {
  return (
    <>
    <div className="grid-container dark">
      <Header />
      <Menu />
      <div className="item3">
        <div className="grid-container-info-page">
          <div className="graph1">
            <WaterChart />
          </div>
          <div className="graph2">
            <PowerChart />
          </div>
          <div className="graph3">
            <CostChart />
          </div>
        </div>
      </div>
    </div>
    </>
  );
};

export default InfoPage;
